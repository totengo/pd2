#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <cstdio>

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include "conhecimento.h"

using namespace cv;
using namespace std;


class Settings{
public:
    Settings() : goodInput(false) {}

    void validate(dados& info){
        goodInput = true;
      /*  cout << "Insira a largura do padrão (nº de pontos na horizontal): ";
        cin >> width;
        cout << "Insira a altura do padrão (nº de pontos na vertical): ";
        cin >> height;
        if (width <= 0 || height <= 0){
            cerr << "Invalid Board size: " << width << " " << height << endl;
            goodInput = false;
        }
        cout << "Insira o comprimento do lado do quadrado do padrão (em milímetros): ";
        cin >> squareSize;
        if (squareSize <= 10e-6){
            cerr << "Invalid square size " << squareSize << endl;
            goodInput = false;
        }
        cout << "Insira o número de snapshots (frames por calibração): ";
        cin >> nrFrames;
        if (nrFrames <= 0){
            cerr << "Invalid number of frames " << nrFrames << endl;
            goodInput = false;
        }
        cout << "Insira o número de frames entre cada snapshot: ";
        cin >> delay;
     	*/
     	width = 9; height = 6; squareSize = 26; nrFrames = 25; delay = 100;
        info.nrFrames = nrFrames;
        
        stringstream ss("0");
        ss >> cameraID;
        inputCapture.open(cameraID);
              
        flag = CALIB_FIX_ASPECT_RATIO | CALIB_FIX_K4 | CALIB_FIX_K5;

        calibrationPattern = CHESSBOARD;
        
        atImageList = 0;

    }

    
public:
    int width, height;           
    enum Pattern {CHESSBOARD};
    Pattern calibrationPattern;  
    float squareSize;            
    int nrFrames;                
    int delay;     
    string outputFileName;       
    int cameraID;
    vector<string> imageList;
    size_t atImageList;
    VideoCapture inputCapture;
    enum InputType {CAMERA};
    InputType inputType;
    bool goodInput;
    int flag;

};

enum { DETECTION = 0, CAPTURING = 1, CALIBRATED = 2 };

bool runCalibrationAndSave(Settings& s, Size imageSize, Mat&  cameraMatrix, Mat& distCoeffs,
                           vector<vector<Point2f> > imagePoints, dados& info );

void calibracam(dados& info, int req){
    Settings s;
    info.g = 0;

    s.validate(info);

    if (!s.goodInput)
        cout << "Invalid input detected. Application stopping. " << endl;


    vector<vector<Point2f> > imagePoints;
    Mat cameraMatrix, distCoeffs;
    Size imageSize;
    int mode = DETECTION;
    clock_t prevTimestamp = 0;
    const Scalar RED(0,0,255), GREEN(0,255,0); 
    const char ESC_KEY = 27;
    char key;

    namedWindow("distorted view", WINDOW_AUTOSIZE);
    namedWindow("undistorted view", WINDOW_AUTOSIZE);

    armazenador armaz1;
    armaz1.id=1;
    armaz1.info=&info;

    armazenador armaz2;
    armaz2.id=2;
    armaz2.info=&info;

    setMouseCallback("distorted view", CallBackFunc, &armaz1);
    setMouseCallback("undistorted view", CallBackFunc, &armaz2);

    while(true){
        Mat view, view2, frame;
        bool blinkOutput = false;

        s.inputCapture >> frame;
        frame.copyTo(view);

        if( mode == CAPTURING && imagePoints.size() >= (size_t)s.nrFrames ){
          if( runCalibrationAndSave(s, imageSize,  cameraMatrix, distCoeffs, imagePoints, info)) {
              info.cameraMatrix=cameraMatrix.clone();
              info.distCoeffs=distCoeffs.clone();
              mode = CALIBRATED;
              info.num_calib++;
              if(info.num_calib==5) {
                if(req == 2)
                    mediariza(info, 2);
              }
          } else
              mode = DETECTION;
        }
        if(view.empty()){
            if( mode != CALIBRATED && !imagePoints.empty() ) {
                runCalibrationAndSave(s, imageSize,  cameraMatrix, distCoeffs, imagePoints, info);
                info.cameraMatrix=cameraMatrix.clone();
                info.distCoeffs=distCoeffs.clone();
            }
            break;
        }

        imageSize = view.size();

        vector<Point2f> pointBuf;

        bool found = false;

        int chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE;
    

        found = findChessboardCorners( view, cvSize(s.width,s.height), pointBuf, chessBoardFlags);
       
 
        if ( found){
                if( s.calibrationPattern == Settings::CHESSBOARD){
                    Mat viewGray;
                    cvtColor(view, viewGray, COLOR_BGR2GRAY);
                    cornerSubPix( viewGray, pointBuf, Size(11,11),
                        Size(-1,-1), TermCriteria( TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1 ));
                }

                if( mode == CAPTURING && (!s.inputCapture.isOpened() || clock() - prevTimestamp > s.delay*1e-3*CLOCKS_PER_SEC) ){
                    imagePoints.push_back(pointBuf);
                    prevTimestamp = clock();
                    blinkOutput = s.inputCapture.isOpened();
                }
                drawChessboardCorners( view, cvSize(s.width, s.height), Mat(pointBuf), found );
        }
       
        string msg = (mode == CAPTURING) ? "100/100" : mode == CALIBRATED ? "Calibrated" : "Press 'g' to start";
        int baseLine = 0;
        Size textSize = getTextSize(msg, 1, 1, 1, &baseLine);
        Point textOrigin(view.cols - 2*textSize.width - 10, view.rows - 2*baseLine - 10);

        if( mode == CAPTURING )
        	msg = format( "%d/%d Snapshot", (int)imagePoints.size(), s.nrFrames ); 

        putText( view, msg, textOrigin, 1, 1, mode == CALIBRATED ?  GREEN : RED);

        if( blinkOutput )
            bitwise_not(view, view);
        
        view2=view.clone();
        if( mode == CALIBRATED ){
            Mat temp = view.clone();
            undistort(temp, view, cameraMatrix, distCoeffs);
        } 
        
        info.exibe[1]=view2.clone();
        info.exibe[2]=view.clone();

        if(req == 2){  
            tracalinha(info, 1);
            tracalinha(info, 2);
            informar(info);
        }

        if(req == 4){ 
            if (mode == CALIBRATED){
                mediariza(info, 4);
                transformacao(info);
                if(info.cont){
                    coordenadasmundo(info);
                }
            }
            tracalinha(info, 1);
            tracalinha(info, 2);
            if(mode == CALIBRATED){
                informar2(info);
            }
        }

        if(mode == CALIBRATED && req == 3){
            Mat *MediaNorma;
            MediaNorma=new Mat[1];
            normatvecs(info, MediaNorma);
            system ("clear");
            cout << endl << "Ditância da câmera à origem do padrão:" << info.norma << endl;
            cout << endl << "(Aperte 'g' para calibrar novamente ou"<< endl <<"feche as janelas de calibração para ver a média e o desvio padrão das distâncias)" << endl;
        }

        imshow("distorted view", info.exibe[1]);
        imshow("undistorted view", info.exibe[2]);
        char key = (char)waitKey(s.inputCapture.isOpened() ? 50 : s.delay);

        if( key  == ESC_KEY )
            break;

        if( s.inputCapture.isOpened() && key == 'g' ){
            ++info.g;
            mode = CAPTURING;
            imagePoints.clear();
            ostringstream oss;
            if(req == 2)
                oss<<"Requisito2_out_camera_data"<<(info.num_calib+1)<<".xml";
            else
                oss<<"Requisito3_out_camera_data"<<(info.num_calib+1)<<".xml";
            s.outputFileName=oss.str();
        }
    }
    destroyAllWindows();
}

static void calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners, Settings::Pattern patternType){
    corners.clear();

    for( int i = 0; i < boardSize.height; ++i )
        for( int j = 0; j < boardSize.width; ++j )
            corners.push_back(Point3f(j*squareSize, i*squareSize, 0));  
}

static bool runCalibration( Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs, vector<vector<Point2f> > imagePoints, vector<Mat>& rvecs, vector<Mat>& tvecs){
  
    cameraMatrix = Mat::eye(3, 3, CV_64F);
    cameraMatrix.at<double>(0,0) = 1;    
    distCoeffs = Mat::zeros(8, 1, CV_64F);

    vector<vector<Point3f> > objectPoints(1);
    calcBoardCornerPositions(cvSize(s.width,s.height), s.squareSize, objectPoints[0], s.calibrationPattern);

    objectPoints.resize(imagePoints.size(),objectPoints[0]);

    //Find intrinsic and extrinsic camera parameters
 	calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, s.flag);  
}

static void saveCameraParams( Settings& s, Size& imageSize, Mat& cameraMatrix, Mat& distCoeffs, const vector<Mat>& rvecs, const vector<Mat>& tvecs, const vector<vector<Point2f> >& imagePoints, dados& info){
    
    FileStorage fs( s.outputFileName, FileStorage::WRITE );

    time_t tm;
    time( &tm );
    struct tm *t2 = localtime( &tm );
    char buf[1024];
    strftime( buf, sizeof(buf), "%c", t2 );

    fs << "Hora_da_calibracao" << buf;
    fs << "Numero_de_snapshots" << s.nrFrames;
    fs << "Largura_da_imagem-em_pixels" << imageSize.width;
    fs << "Altura_da_imagem-em_pixels" << imageSize.height;
    fs << "Largura_do_padrao-pontos_na_horizontal" << s.width;
    fs << "Altura_do_padrao-pontos_na_vertical" << s.height;
    fs << "Lado_do_quadrado_do_padrao-em_milimetros" << s.squareSize;
    fs << "Parametros_intrinsecos" << cameraMatrix;
    fs << "Coeficientes_de_distorcao" << distCoeffs;

   if(!rvecs.empty() && !tvecs.empty() )
    {
        info.rmat = rvecs;
        for(int i = 0; i < s.nrFrames-1; ++i){
            Rodrigues(rvecs[i], info.rmat[i], noArray());
        }
        fs << "Vetor_de_translacao-parametros_extrinsecos" << tvecs;
        fs << "Matriz_de_rotacao-parametros_extrinsecos" << info.rmat;
    }
}

bool runCalibrationAndSave(Settings& s, Size imageSize, Mat& cameraMatrix, Mat& distCoeffs, vector<vector<Point2f> > imagePoints, dados& info){
    vector<Mat> rvecs, tvecs;

    bool ok = runCalibration(s, imageSize, cameraMatrix, distCoeffs, imagePoints, rvecs, tvecs);
    info.tvecs=tvecs; 
    info.rvecs=rvecs;
   
    saveCameraParams(s, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, imagePoints, info);

    return ok;
}