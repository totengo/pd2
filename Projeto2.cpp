#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <math.h>

using namespace cv;
using namespace std;

Mat imagem, img;
int rows1, cols1, rows2, cols2, click=0, aux=0;
float cont;
char objeto [50];


void mouseEvent(int evt, int x, int y, int flags, void* param){      
	int i,j;
	float d;          
    Mat* rgb = (Mat*) param;
    if (evt == CV_EVENT_LBUTTONDOWN){ 
    	cont++;
    	aux = cont/2;
        if(aux-cont/2 != 0){
        	rows1 = y;
        	cols1 = x;
        }else{
        	rows2 = y;
        	cols2 = x;
        }
        system("clear");
        printf("*Distância*\n");
        printf("\nEscolha o ponto 2\n");
//      printf("rows1 = %d, cols1 = %d e rows2 = %d, cols2 = %d",rows1,cols1,rows2,cols2);
 	    if(aux-cont/2 == 0){
 	    	line(img, Point (cols1,rows1), Point (cols2,rows2), 255, 2, 20, 0);
/*      	if(rows1-rows2 > 0){
	        	if(cols1-cols2 > 0){
	        			j = cols2;
	        		for (i = rows2; i <rows1; i++){	
	        				j++;
							(img).at<Vec3b>(i, j)[0] = 0;
							(img).at<Vec3b>(i, j)[1] = 255;
							(img).at<Vec3b>(i, j)[2] = 0;
							
					}
	        	}else{
	        			j = cols2;
	        		for (i = rows2; i <rows1; i++){ 
	        				j--;			    		
							(img).at<Vec3b>(i, j)[0] = 0;
							(img).at<Vec3b>(i, j)[1] = 255;
							(img).at<Vec3b>(i, j)[2] = 0;
							
					}
	        	}
	        }else{
	        	if(cols1-cols2 > 0){
	        		j = cols2;
	        		for (i = rows1; i <rows2; i++){
	        				j--; 
							(img).at<Vec3b>(i, j)[0] = 0;
							(img).at<Vec3b>(i, j)[1] = 255;
							(img).at<Vec3b>(i, j)[2] = 0;
							
					}
	        	}else{
	        		j = cols2;
	        		for (i = rows1; i <rows2; i++){ 
	        				j++;			    		
							(img).at<Vec3b>(i, j)[0] = 0;
							(img).at<Vec3b>(i, j)[1] = 255;
							(img).at<Vec3b>(i, j)[2] = 0;
							
					}
	        	}
	        }
			*/	
			system("clear");		
			printf("*Distância*\n");
			d = sqrt((rows1-rows2)*(rows1-rows2)+(cols1-cols2)*(cols1-cols2));
		    printf("\nDistância entre os pixels: %f\n", d);
    		printf("\nEscolha de novo o ponto 1\n");
		}
		imshow("Open Window", img);
		img = imagem.clone();
	}
}


void mouseclickImage (){
	int j, i;
	imagem = imread(objeto);
	if ( !imagem.data ){
        cout << "Imagem não encontrada!" << endl;
        exit(0);
    }
	img = imagem.clone();
	namedWindow("Open Window", WINDOW_AUTOSIZE);
	system("clear");
	printf("*Distância*\n");
	printf("\nEscolha o ponto 1\n");
	setMouseCallback("Open Window", mouseEvent, &imagem);
	imshow("Open Window", img);
	waitKey(0);
	destroyAllWindows();
}





char menu1(){
	system("clear");
	printf("==================\n");
	printf("=      Menu      =\n");
	printf("==================\n");
	printf("\n1) Requisito 1\n2) Requisito 2\n3) Requisito 3\n4) Requisito 4\n5) Sair\n\nEscolha uma Opção: ");
	char i[20];
	scanf("%[^\n]", i);
	while(getchar()!='\n');
	if(i[1]=='\0' && (i[0]=='1' || i[0]=='2' || i[0]=='3' || i[0]=='4' || i[0]=='5')){
		return i[0];
	}else{
		return 0;
	}
}


int main(){	
	char escolha='0';
		while(escolha!='5'){
			if(escolha=='0'){
				escolha=menu1();
			}
			if(escolha=='1'){
				system("clear");
				printf("*Distância*\n");
				printf("\nDigite o nome da imagem .jpg: ");
				scanf("%s", objeto);
				while(getchar()!='\n');
				mouseclickImage ();
				escolha = '0';
			}
			if(escolha=='2'){
				escolha = '0';
			}
			if(escolha=='3'){
				escolha = '0';
			}
			if(escolha=='4'){
				escolha = '0';
			}
			if(escolha!='5'){
				escolha = '0';
			}			
		}	
	return 0;
}
