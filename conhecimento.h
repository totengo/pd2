#ifndef CONHECIMENTO_H
#define CONHECIMENTO_H
	
	#include <stdio.h>
	#include <stdlib.h>
	#include <opencv2/opencv.hpp>
	#include <opencv2/core.hpp>
	#include <opencv2/core/utility.hpp>
	#include "opencv2/imgcodecs.hpp"
	#include <opencv2/highgui.hpp>
	#include <opencv2/highgui/highgui.hpp>
	#include <opencv2/imgproc.hpp>
	#include <iostream>
	#include <sstream>
	#include <math.h>

	#define ACESSA(x, tipo) ((*((tipo*)(x))))

	
	using namespace std;
	using namespace cv;

	typedef struct dados { 
		vector <Mat> rvecs, tvecs, rmat;
		vector <Mat> mediatvecs, mediarvecs, mediarmat;
		int nrFrames;
		float norma, resultmedianormas, resultdesvionormas;
		int g;
		bool calibrou;
		int num_calib;
		Mat matP, invP, C1, C2;
		Mat cameraMatrix, distCoeffs, matRt;
		Mat DPCam, DPCoeffs;
		Mat* exibe;
		int* estado;
		int* old_estado;
		int* completude;
		int* pronto; 
		int* coordenadas;
		float* coordMundo;
		int* aerial;
		int serv;
		int sair;
		int cont;
		int p1[2], P1[2];
		int p2[2], P2[2];
		int aux;
		float tamanho;
	} dados;

	typedef struct armazenador {
		int id;
		dados* info; 
	} armazenador;

	//protótipos de funcoes
	void menu(dados& info);
	void requisito1(dados& info);
	void requisito3(dados& info, int req);
	void requisito4(dados& info, int req);
	void tracalinha(dados& info, int num);
	void informar(dados& info);
	void informar2(dados& info);
	void CallBackFunc(int event, int x, int y, int flags, void* userdata);
	void calibracam(dados& info, int req);
	void inicializa(dados& info);
	void mediariza(dados& info, int req);
	void media (Mat *Matriz, Mat *Media);
	void desviopadrao (Mat *Matriz, Mat *DesvioPadrao, Mat *Media);
	void normatvecs(dados& info, Mat *MediaNorma);
	void mediarvecs(dados& info, Mat *MediaNorma);
	void transformacao(dados& info);
	void coordenadasmundo(dados& info);

#endif