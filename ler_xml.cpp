#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <cstdio>
#include <math.h>
#include <stdlib.h>

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "conhecimento.h"


using namespace std;
using namespace cv;



void mediariza(dados& info, int req){
    Mat *CameraMatrix, *DistortionCoefficients, *Media, *DesvioPadrao, *MediaNorma, *Mediarvecs;
    Mediarvecs = new Mat[1];
    MediaNorma = new Mat[1];
    CameraMatrix = new Mat[5];
    DistortionCoefficients=new Mat[5];
    Media=new Mat();
    DesvioPadrao=new Mat();
    FileStorage fs;
    if(req == 2){
        for (int i=0;i<5;++i) {

            std::ostringstream oss;
            oss<<"Requisito2_out_camera_data"<<i+1<<".xml";

            fs.open(oss.str(), FileStorage::READ);

            fs["Parametros_intrinsecos"] >> CameraMatrix[i];
            fs["Coeficientes_de_distorcao"] >> DistortionCoefficients[i];

            fs.release();
        }
        media(CameraMatrix, Media);
        info.cameraMatrix=(*Media).clone();
        media(DistortionCoefficients, Media);
        info.distCoeffs=(*Media).clone();

        desviopadrao(CameraMatrix, DesvioPadrao, &(info.cameraMatrix));
        info.DPCam=(*DesvioPadrao).clone();
        desviopadrao(DistortionCoefficients, DesvioPadrao, &(info.distCoeffs));
        info.DPCoeffs=(*DesvioPadrao).clone();

        FileStorage fw("intrisics.xml", FileStorage::WRITE);
        fw<<"Media_dos_intrinsecos"<<info.cameraMatrix;
        fw<<"DP_dos_intrinsecos"<<info.DPCam;

        fw.release();

        fw=FileStorage("distortion.xml", FileStorage::WRITE);
        fw<<"Media_dos_coeficientes_de_distorcao"<<info.distCoeffs;
        fw<<"DP_dos_coeficientes_de_distorcao"<<info.DPCoeffs;

        fw.release(); 
    }
    if(req == 3){
        float medianorma[info.g];
        for (int j=0; j < info.g; ++j) {
            std::ostringstream oss;
            oss<<"Requisito3_out_camera_data"<<j+1<<".xml";

            fs.open(oss.str(), FileStorage::READ);

            fs["Vetor_de_translacao-parametros_extrinsecos"] >> info.tvecs;

            fs.release();

            normatvecs(info, MediaNorma);
            medianorma[j] = info.norma;
        }
        float resultado = 0; 
        for (int i=0; i < info.g; ++i) {
            resultado += medianorma[i];
        }    
        info.resultmedianormas= resultado/info.g;
        resultado = 0;
        for (int i=0; i < info.g; ++i) {
            resultado += (medianorma[i] - info.resultmedianormas)*(medianorma[i] - info.resultmedianormas);
        }
        info.resultdesvionormas= sqrt(resultado)/sqrt(info.g);
    }
    if(req == 4){
        mediarvecs(info, Mediarvecs);
        info.mediarvecs = info.rvecs;
        info.mediarvecs[0] = Mediarvecs[0];
        normatvecs(info, MediaNorma);
        info.mediatvecs = info.tvecs;
        info.mediatvecs[0] = MediaNorma[0];

    }
}

void media (Mat *Matriz, Mat *Media){
    (*Media)=Matriz[0].clone();
    MatIterator_<double> it[6], end[6];
    it[5]=(*Media).begin<double>();
    end[5]=(*Media).end<double>();
    for(int i=0;i<5;++i) {
        it[i]=Matriz[i].begin<double>();
        end[i]=Matriz[i].end<double>();
    }
    for(;it[5]!=end[5];++it[0], ++it[1], ++it[2], ++it[3], ++it[4], ++it[5]) {
        (*it[5])=(*it[0])+(*it[1])+(*it[2])+(*it[3])+(*it[4]);
        (*it[5])/=5;
    }
}

void desviopadrao (Mat *Matriz, Mat *DesvioPadrao, Mat *Media){
    double elem[5];
    (*DesvioPadrao)=Matriz[0].clone();
    MatIterator_<double> it[7], end[7];
    it[6]=(*Media).begin<double>();
    end[6]=(*Media).end<double>();
    it[5]=(*DesvioPadrao).begin<double>();
    end[5]=(*DesvioPadrao).end<double>();
    for(int i=0;i<5;++i) {
        it[i]=Matriz[i].begin<double>();
        end[i]=Matriz[i].end<double>();
    }
    for(;it[5]!=end[5];++it[0], ++it[1], ++it[2], ++it[3], ++it[4], ++it[5], ++it[6]) {
        elem[0] = (((*it[0])-(*it[6]))*((*it[0])-(*it[6])));
        elem[1] = (((*it[1])-(*it[6]))*((*it[1])-(*it[6])));
        elem[2] = (((*it[2])-(*it[6]))*((*it[2])-(*it[6])));
        elem[3] = (((*it[3])-(*it[6]))*((*it[3])-(*it[6])));
        elem[4] = (((*it[4])-(*it[6]))*((*it[4])-(*it[6])));
        (*it[5])=sqrt(elem[0]+elem[1]+elem[2]+elem[3]+elem[4]);
        (*it[5])/=sqrt(5);
    }
}

void normatvecs(dados& info, Mat *MediaNorma){
    int n=0;
    float norma=0;
    n = info.nrFrames;
    (MediaNorma[0])=(info.tvecs[0]).clone();
    MatIterator_<double> it[n+1], end[n+1];
    it[0]=(MediaNorma[0]).begin<double>();
    end[0]=(MediaNorma[0]).end<double>();
    for(int i=1;i<n+1;++i) {
        it[i]=info.tvecs[i-1].begin<double>();
        end[i]=info.tvecs[i-1].end<double>();
    }
    while(it[0]!=end[0]){
        for(int j=0;j<n+1;++j){
            ++it[j];
        }
        for(int k=1;k<n+1;++k){
            (*it[0])+=(*it[k]);
        }
        (*it[0])/=n;
    }
    it[0]=(*MediaNorma).begin<double>();
    for(;it[0]!=end[0];++it[0]){
        norma += (*it[0])*(*it[0]);
    }
    norma=sqrt(norma);
    info.norma = norma; 
}

void mediarvecs(dados& info, Mat *Mediarvecs){
    int n=0;
    n = info.nrFrames;
    (Mediarvecs[0])=(info.rvecs[0]).clone();
    MatIterator_<double> it[n+1], end[n+1];
    it[0]=(Mediarvecs[0]).begin<double>();
    end[0]=(Mediarvecs[0]).end<double>();
    for(int i=1;i<n+1;++i) {
        it[i]=info.rvecs[i-1].begin<double>();
        end[i]=info.rvecs[i-1].end<double>();
    }
    while(it[0]!=end[0]){
        for(int j=0;j<n+1;++j){
            ++it[j];
        }
        for(int k=1;k<n+1;++k){
            (*it[0])+=(*it[k]);
        }
        (*it[0])/=n;
    } 
}