#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include "opencv2/imgcodecs.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <iostream>
#include <sstream>
#include <math.h>
#include "conhecimento.h"



using namespace cv;
using namespace std;

void tracalinha(dados& info, int num) {
	if(num == 1 || num == 0){
		if(info.completude[num]) {
			line(info.exibe[num], Point(info.coordenadas[0+4*num], info.coordenadas[1+4*num]), Point(info.coordenadas[2+4*num], info.coordenadas[3+4*num]), Scalar(0, 0, 255), 2, 20);
		} else if(info.pronto[num]) {
			line(info.exibe[num], Point(info.coordenadas[0+4*num], info.coordenadas[1+4*num]), Point(info.aerial[0+2*num], info.aerial[1+2*num]), Scalar(0, 0, 255), 2, 20);
		}
	}else{
		if(info.completude[num]) {
			line(info.exibe[num], Point(info.coordenadas[0+4*num], info.coordenadas[1+4*num]), Point(info.coordenadas[2+4*num], info.coordenadas[3+4*num]), Scalar(0, 255, 0), 2, 20);
		} else if(info.pronto[num]) {
			line(info.exibe[num], Point(info.coordenadas[0+4*num], info.coordenadas[1+4*num]), Point(info.aerial[0+2*num], info.aerial[1+2*num]), Scalar(0, 255, 0), 2, 20);
		}
	}
}

void CallBackFunc(int event, int x, int y, int flags, void* userdata) {
	armazenador armaz=ACESSA(userdata, armazenador);
	int id=armaz.id;
	dados& info=*armaz.info;
	info.aerial[0+2*id]=x;
	info.aerial[1+2*id]=y;
	if ( event == EVENT_LBUTTONDOWN ) {
		info.pronto[0+id]=1;
		info.coordenadas[info.estado[0+id]*2+0+4*id]=x;
	    info.coordenadas[info.estado[0+id]*2+1+4*id]=y;
		info.completude[0+id]=info.estado[0+id];
		info.estado[0+id]=1-info.estado[0+id];
		if(info.aux != 0){
			info.cont = 1 - info.cont;
		}
		++info.aux;
	}
}

void informar(dados& info) {
	for(int i=0;i<info.serv;++i) {
		if(info.old_estado[i]!=info.estado[i]) {
			system("clear");
			if(!info.estado[i]&&info.completude[i]) {
				cout<<"distância entre os pontos: "<<sqrt((info.coordenadas[0+4*i]-info.coordenadas[2+4*i])*(info.coordenadas[0+4*i]-info.coordenadas[2+4*i])+(info.coordenadas[1+4*i]-info.coordenadas[3+4*i])*(info.coordenadas[1+4*i]-info.coordenadas[3+4*i]))<<" na janela "<<i<<endl;
				if((i==1||i==2)&&!info.estado[3-i]&&info.completude[3-i]) {
					i=3-i;
					cout<<"distância entre os pontos: "<<sqrt((info.coordenadas[0+4*i]-info.coordenadas[2+4*i])*(info.coordenadas[0+4*i]-info.coordenadas[2+4*i])+(info.coordenadas[1+4*i]-info.coordenadas[3+4*i])*(info.coordenadas[1+4*i]-info.coordenadas[3+4*i]))<<" na janela "<<i<<endl;
					i=3-i;	
				}
			}
			cout<<"Escolha o ponto "<<((info.estado[i])?2:1)<<" para a janela "<<i<<endl;
			info.old_estado[i]=info.estado[i];
		}
	}
}

void informar2(dados& info) {
	for(int i=0;i<info.serv;++i) {
		if(info.old_estado[i]!=info.estado[i]) {
			system("clear");
			if(!info.estado[i]&&info.completude[i]) {
				info.tamanho=0;
				MatIterator_<double> it[2], end[2];
				it[0]=(info.C1).begin<double>();
			 	end[0]=(info.C1).end<double>();
			    it[1]=(info.C2).begin<double>();
			    end[1]=(info.C2).end<double>();
			    for(int j = 0; j < 3 ; ++j, ++it[0], ++it[1]){
			    	info.tamanho += (*it[0])*(*it[1]);
			    }
			    info.tamanho = sqrt(info.tamanho);
			    if((i==1||i==2)&&!info.estado[3-i]&&info.completude[3-i]) {
			    	cout<< endl << "O tamanho real do objeto é: "<< info.tamanho << endl;
			    }
			}
			cout<< endl << "Escolha novos pontos de medida!";
		}
	}
}

void requisito1(dados& info) {
	char c;
	armazenador armaz0;
	armaz0.id=0;
	armaz0.info=&info;
	namedWindow("requisito_1", WINDOW_AUTOSIZE);
	setMouseCallback("requisito_1", CallBackFunc, &armaz0);
	VideoCapture cap(0);

	do {
		cap>>(*info.exibe);

		tracalinha(info, 0);

		informar(info);

		imshow("requisito_1", info.exibe[0]);

		c=waitKey(30);
	} while(c!=27);
	destroyAllWindows();
}

void requisito3(dados& info, int req){
	calibracam(info, 3);
	mediariza(info, 3);
	char c;
	while(c != 'q'){
		system("clear");
		cout<< endl << "Média das " << info.g << " distâncias calculadas:" << info.resultmedianormas << endl;
		cout<< "Desvio padrão das " << info.g << " distâncias calculadas:" << info.resultdesvionormas << endl;
		cout<< endl << "Digite 'q' e aperte 'enter' para voltar ao menu"<<endl;
		c = getchar();
	}
}

void requisito4(dados& info, int req){
	info.p1[0]=0;
	info.p1[1]=0;
	info.p2[0]=0;
	info.p2[1]=0;
	info.cont = 0;
	info.aux = 0;
	calibracam(info, 4);
}

void transformacao(dados& info){
	info.matRt= Mat::zeros(3,4, CV_64F);
	info.mediarmat = info.mediarvecs;
    Rodrigues(info.mediarvecs[0], info.mediarmat[0], noArray());
	MatIterator_<double> it[3], end[3];
	it[0]=(info.matRt).begin<double>();
    end[0]=(info.matRt).end<double>();
    it[1]=(info.mediarmat[0]).begin<double>();
    end[1]=(info.mediarmat[0]).end<double>();
    it[2]=(info.mediatvecs[0]).begin<double>();
    end[2]=(info.mediatvecs[0]).end<double>();
    int contador=0;
    for(; it[0]!=end[0]; ++it[0]){
    	++contador;
  	 	if(contador == 4 || contador == 8 || contador == 12){
  	 		(*it[0])=(*it[2]);
  	 		++it[2];
  	 	}else{
  	 		(*it[0])=(*it[1]);
  	 		++it[1];
  	 	}
    }
}

void coordenadasmundo(dados& info){
	Mat ci1, ci2;
	ci1 = (Mat_<double>(3,1) << info.coordenadas[4], info.coordenadas[5], 0);
	info.C1 = (Mat_<double>(4,1) << 0,0,0,0);
	ci2 = (Mat_<double>(3,1) << info.coordenadas[6], info.coordenadas[7], 0);
	info.C2 = (Mat_<double>(4,1) << 0,0,0,0);
	info.matP = (Mat_<double>(3,4) << 0,0,0,0,0,0,0,0,0);
	info.invP = (Mat_<double>(4,3) << 0,0,0,0,0,0,0,0,0);
	info.matP = (info.cameraMatrix)*(info.matRt);
	info.invP = (info.matP).inv(DECOMP_SVD);
	info.C1 = (info.invP)*(ci1);
	info.C2 = (info.invP)*(ci2);
}

void menu(dados& info) {
	system("clear");
	cout<<"Menu Principal"<<endl<<endl<<"1) Requisito 1"<<endl<<"2) Requisito 2"<<endl<<"3) Requisito 3"<<endl<<"4) Requisito 4"<<endl<<"5) Sair"<<endl<<endl<<"Escolha uma opção: ";
	int option;
	cin>>option;
	switch (option) {
		case 1: requisito1(info); break;
		case 2: calibracam(info, 2); break;
		case 3: requisito3(info, 3); break;
		//case 4: coordenadasmundo(info); break;
		case 4: requisito4(info, 4); break;
		case 5: info.sair=1; break;
		default: break;
	}
}

void inicializa(dados& info) {
	for(int i=0;i<info.serv;++i) {
		info.estado[i]=0;
		info.old_estado[i]=1;
		info.completude[i]=0;
		info.pronto[i]=0;
	}
	info.num_calib=0;
}

int main(int argc, char** argv) {
	int serv=3;
	char c;
	dados info;
	info.serv=serv;
	info.coordenadas=(int*)malloc(sizeof(int)*4*serv);
	info.aerial=(int*)malloc(sizeof(int)*2*serv);
	info.estado=(int*)malloc(sizeof(int)*serv);
	info.old_estado=(int*)malloc(sizeof(int)*serv);
	info.completude=(int*)malloc(sizeof(int)*serv);
	info.pronto=(int*)malloc(sizeof(int)*serv);
	info.exibe=new Mat[serv];
	info.sair=0;
	inicializa(info);
	do {
		menu(info);
		c=waitKey(2);
	} while(c!=27&&!info.sair);
	free(info.coordenadas);
	free(info.aerial);
	free(info.estado);
	free(info.old_estado);
	free(info.completude);
	free(info.pronto);
	delete[] info.exibe;
	return 0;
}